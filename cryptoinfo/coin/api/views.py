from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    UpdateModelMixin,
)
from cryptoinfo.coin.models import Coin
from cryptoinfo.coin.api.serializers import CoinSerializer


class CoinViewSet(
    DestroyModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
    CreateModelMixin,
    ListModelMixin,
    GenericViewSet,
):
    def get_queryset(self):
        return Coin.objects.all()

    def get_serializer_class(self):
        return CoinSerializer
