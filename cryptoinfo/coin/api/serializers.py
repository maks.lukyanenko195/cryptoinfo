from rest_framework.serializers import ModelSerializer
from cryptoinfo.coin.models import Coin


class CoinSerializer(ModelSerializer):
    class Meta:
        model = Coin
        fields = '__all__'
