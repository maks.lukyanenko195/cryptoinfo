from django.contrib import admin
from cryptoinfo.coin.models import Coin


class CoinAdmin(admin.ModelAdmin):
    pass


admin.site.register(Coin, CoinAdmin)
