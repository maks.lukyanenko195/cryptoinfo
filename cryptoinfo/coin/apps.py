from django.apps import AppConfig


class CoinConfig(AppConfig):
    name = "cryptoinfo.coin"
