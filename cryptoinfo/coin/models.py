from django.db import models


class Coin(models.Model):
    title = models.CharField(max_length=30)
    description = models.TextField()
    price = models.DecimalField(max_digits=999, decimal_places=8)
    capitalization = models.DecimalField(max_digits=999, decimal_places=4)
    volume = models.DecimalField(max_digits=999, decimal_places=4)

    class Meta:
        verbose_name = 'coin'
        verbose_name_plural = 'coins'
