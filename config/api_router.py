from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from cryptoinfo.users.api.views import UserViewSet
from cryptoinfo.coin.api.views import CoinViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("coins", CoinViewSet, basename="coin")


app_name = "api"
urlpatterns = router.urls
